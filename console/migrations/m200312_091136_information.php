<?php

use yii\db\Migration;

/**
 * Class m200312_091136_information
 */
class m200312_091136_information extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200312_091136_information cannot be reverted.\n";

        return false;
    }

    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%information}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(300),
            'user_id' => $this->integer(11),
            'category_id' => $this->integer(11),
            'seo_keywords' => $this->string(300),
            'slug' => $this->string(300),
            'seo_description' => $this->string(300),
            'short_description' => $this->string(300),
            'description' => $this->text(),
            'img_name'=>$this->string(300),
            'created_at' => $this->string(300),
            'updated_at' => $this->string(300),
            'is_status' => $this->tinyInteger(2),

        ], $tableOptions);

        $this->addForeignKey(
            'FK_information_user',
            'information',
            'user_id',
            'user',
            'id',
            'CASCADE',
            'CASCADE'
        );

        $this->addForeignKey(
            'FK_information_category',
            'information',
            'category_id',
            'categories',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }
    public function down()
    {
        $this->dropTable('{{%information}}');
    }
}
