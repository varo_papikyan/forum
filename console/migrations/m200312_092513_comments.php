<?php

use yii\db\Migration;

/**
 * Class m200312_092513_comments
 */
class m200312_092513_comments extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200312_092513_comments cannot be reverted.\n";

        return false;
    }

    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%comments}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(11),
            'information_id' => $this->integer(11),
            'comment' => $this->text(),
            'created_at' => $this->string(300),
            'updated_at' => $this->string(300),
            'is_status' => $this->tinyInteger(2),

        ], $tableOptions);
        $this->addForeignKey(
            'FK_comments_user',
            'comments',
            'user_id',
            'user',
            'id',
            'CASCADE',
            'CASCADE'
        );

        $this->addForeignKey(
            'FK_comments_information',
            'comments',
            'information_id',
            'information',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    public function down()
    {
        $this->dropTable('{{%comments}}');
    }

}
