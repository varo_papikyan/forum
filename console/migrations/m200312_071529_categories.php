<?php

use yii\db\Migration;

/**
 * Class m200312_071529_categories
 */
class m200312_071529_categories extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200312_071529_categories cannot be reverted.\n";

        return false;
    }

    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%categories}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(300),
            'slug' => $this->string(300),
            'seo_keywords' => $this->string(300),
            'seo_description' => $this->string(300),
            'short_description' => $this->string(300),
            'description' => $this->text(),
            'img_name'=>$this->string(300),
            'created_at' => $this->string(300),
            'updated_at' => $this->string(300),
            'is_status' => $this->tinyInteger(2),

        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('{{%categories}}');
    }
}
