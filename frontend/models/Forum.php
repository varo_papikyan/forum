<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "Pages".
 *
 * @property int $id
 * @property string|null $name
 * @property int|null $user_id
 * @property string|null $short_description
 * @property string|null $description
 * @property string|null $img_name
 * @property int|null $is_status
 * @property string|null $created_at
 * @property string|null $updated_at
 */
class Forum extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'Pages';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'is_status'], 'integer'],
            [['description'], 'string'],
            [['name'], 'string', 'max' => 255],
            [['short_description', 'img_name', 'created_at', 'updated_at'], 'string', 'max' => 300],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'user_id' => 'User ID',
            'short_description' => 'Short Description',
            'description' => 'Description',
            'img_name' => 'Img Name',
            'is_status' => 'Is Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
