<?php

/* @var $this yii\web\View */

$this->title = 'Program Information';

?>
<div class="row">
    <?php

    foreach ($categories as $category) {
        ?>

        <div class="col-md-4">
            <a href="<?= $homeUrl ?>categories/index?slug=<?= $category['name'] ?>">
                         <?= $category['name'] ?></a>

            <div class="col-xs-12">
                <?= $category['short_description'] ?>
            </div>
        </div>

        <?php
    }
    ?>
</div>