<?php $homeUrl = Yii::$app->homeUrl; ?>
<div class="wrapper-main">
    <!-- Top Bar -->
    <div class="top-bar">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div class="social-media">
                        <ul>
                            <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                            <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fab fa-google-plus-g"></i></a></li>
                            <li><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
                            <li><a href="#"><i class="fab fa-instagram"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-6">

                </div>
            </div>
        </div>
    </div>
    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-dark bg-light top-nav">
        <div class="container">
            <a class="navbar-brand" href="<?= $homeUrl ?>">
                <img src="<?= $homeUrl ?>images/logo.png" width="200px"  alt="logo"/>
            </a>
            <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse"
                    data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false"
                    aria-label="Toggle navigation">
                <span class="fas fa-bars"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarResponsive">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item">
                        <a class="nav-link " href="<?= $homeUrl ?>">Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?= $homeUrl ?>pages/about">About</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?= $homeUrl ?>categories/index">Categories</a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link" href="<?= $homeUrl ?>pages/contact">Contact</a>
                    </li>

                    <?php
                    if (Yii::$app->user->isGuest) {
                        ?>
                        <li class="nav-item">
                            <a class="nav-link" href="<?= $homeUrl ?>site/signup">Registration</a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link" href="<?= $homeUrl ?>site/login">Login</a>
                        </li>
                        <?php
                    } else {
                        ?>
                        <li class="nav-item">
                            <a class="nav-link" href="<?= $homeUrl ?>user/index">acount
                                ( <?= Yii::$app->user->identity->username ?> )</a>
                        </li>
                        <?php
                    }
                    ?>
                </ul>
            </div>
        </div>
    </nav>

</div>
