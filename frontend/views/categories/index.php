<?php

$homeUrl = Yii::$app->homeUrl;
?>

<div class="full-title">
    <div class="container">
        <h1 class="mt-4 mb-3"> Categories </h1>
        <div class="breadcrumb-main">
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="<?= $homeUrl ?>">Home</a>
                </li>
                <li class="breadcrumb-item active">Categories</li>
            </ol>
        </div>
    </div>
</div>

<div class="portfolio-col">
    <div class="container">
        <div class="row">
            <?php foreach ($categories as $value) { ?>
                <div class="col-lg-4 col-sm-6 portfolio-item">
                    <div class="card h-100">
                        <a class="hover-box" href="<?= $homeUrl ?>categories/details?slug=<?= $value['slug'] ?>">
                            <div class="dot-full">
                                <i class="fas fa-link"></i>
                            </div>
                            <img class="card-img-top"
                                 src="<?= $homeUrl ?>images/categories/small/<?= $value['img_name'] ?>" alt=""/>
                        </a>
                        <div class="card-body">
                            <h4>
                                <a href="<?= $homeUrl ?>categories/details?slug=<?= $value['slug'] ?>"><?= $value['name'] ?></a>
                            </h4>
                            <p><?= $value['short_description'] ?></p>
                        </div>
                    </div>
                </div>
                <?php
            }
            ?>
        </div>

    </div>
</div>


