<?php
$homeUrl = Yii::$app->homeUrl;

use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>



<?php

if (!Yii::$app->user->isGuest) {

    $form = ActiveForm::begin(); ?>


    <div class="form-group">
    </div>

    <?php

    ActiveForm::end();

}
?>
<div class="full-title">
    <div class="container">
        <h1 class="mt-4 mb-3"> Information </h1>
        <div class="breadcrumb-main">
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="<?= $homeUrl ?>">Home</a>
                </li>
                <li class="breadcrumb-item active"> Information</li>
            </ol>
        </div>
    </div>
</div>

<div class="portfolio-col">
    <div class="container">
        <div class="col-xs-12">
            <h1><?= $information['name'] ?></h1>
        </div>
        <div class="row">

            <div class="col-xs-6">


                <img src="<?= $homeUrl ?>images/information/small/<?= $information['img_name'] ?>" alt="">
            </div>
            <div class="col-xs-6">
                <p> <?= $information['description'] ?> </p>
            </div>
        </div>
    </div>

</div>
<hr>


<div class="blog-main">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">

                <div class="blog-right-side">
                    <!-- Comments Form -->
                    <div class="card my-4">
                        <h5 class="card-header">Leave a Comment:</h5>
                        <div class="card-body">
                            <?php $form = ActiveForm::begin(); ?>
                            <?= $form->field($comments, 'comment')->textarea(['row' => 8]) ?>


                            <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-primary']) ?>

                            <?php

                            ActiveForm::end();
                            ?>

                        </div>
                    </div>
                </div>

                <?php
                foreach ($selectComments as $comment) {
                    ?>
                    <div class="media mb-4">
                        <img class="d-flex mr-3 rounded-circle" src="<?= $homeUrl ?>images/testi_01.png" alt="">
                        <div class="media-body">

                            <h5 class="mt-0"><?= $comment['user']['username']?></h5>
                        <?= $comment['comment']?>
                        </div>
                    </div>
                    <?php
                }
             ?>
            </div>
        </div>
    </div>
</div>
