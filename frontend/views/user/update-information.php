
<?php
$homeUrl = Yii::$app->homeUrl;

use kartik\switchinput\SwitchInput;
use kartik\widgets\FileInput;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\redactor\widgets\Redactor;
use yii\widgets\ActiveForm;

?>
<div class="full-title">
    <div class="container">
        <h1 class="mt-4 mb-3"> Update Information </h1>
        <div class="breadcrumb-main">
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="<?= $homeUrl ?>">Home</a>
                </li>
                <li class="breadcrumb-item active"><?= Yii::$app->user->identity['username'] ?></li>
            </ol>
        </div>
    </div>
</div>

<div class="container">

    <?php $form = ActiveForm::begin(); ?>

    <div class="col-xs-12">
        <div class="row">
            <div class="col-xs-6">
                <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-xs-6">
                <?= $form->field($model, 'category_id')->widget(
                    \kartik\select2\Select2::classname(), [
                    'data' =>
                        \yii\helpers\ArrayHelper::map(
                            \common\models\Categories::find()
                                ->select('id,name')
                                ->asArray()
                                ->all(),
                            "id", "name"),
                    'options' => ['placeholder' => 'Parent'],
                    'pluginOptions' => ['allowClear' => true],
                ])
                ;
                ?>
            </div>

        </div>
    </div>
    <div class="col-xs-12">
        <div class="row">
            <div class="col-xs-6">
                <?php

                $modelId = $model->id;
                $uploadImagesUrl = Url::to(['/categories/upload-images?id=' . $modelId]);

                $imagesOptions = [];
                $imgPath = [];

                if (!$model->isNewRecord) {
                    $imgName = $model->img_name;
                    $imgFullPath = Yii::getAlias("@frontend") . "/web/images/categories/" . $imgName;

                    if (!empty($imgName)) {
                        $deleteUrl = Url::to(["/categories/delete-file?id=" . $modelId]);

                        $imgPath[] = Url::to('http://' . $_SERVER['HTTP_HOST'] . '/images/blog/') . $imgName;
                        $size = 0;
                        if (file_exists($imgFullPath)) {
                            $size = filesize($imgFullPath);
                        }
                        $imagesOptions[] = [
//                'caption' => $model->title,
                            'url' => $deleteUrl,
                            'size' => $size,
                            'key' => $modelId,
                        ];
                    }
                }
                ?>

                <?=
                $form->field($model, 'img_name')
                    ->widget(
                        FileInput::class,
                        [
                            'attribute' => 'img_name',
                            'name' => 'img_name',
                            'options' =>
                                [
                                    'accept' => 'image/*',
                                    'multiple' => false,
                                ],
                            'pluginOptions' =>
                                [
                                    'previewFileType' => 'image',
                                    "uploadAsync" => true,
                                    'showPreview' => true,
                                    'showUpload' => $model->isNewRecord ? false : true,
                                    'showCaption' => false,
                                    'showDrag' => false,
                                    'uploadUrl' => $uploadImagesUrl,
                                    'initialPreviewConfig' => $imagesOptions,
                                    'initialPreview' => $imgPath,
                                    'initialPreviewAsData' => true,
                                    'initialPreviewShowDelete' => true,
                                    'overwriteInitial' => true,
                                    'resizeImages' => true,
                                    'layoutTemplates' => [!$model->isNewRecord ?: 'actionUpload' => '',],
                                ],
                        ]);
                ?>
            </div>
            <div class="col-xs-6">
                <?= $form->field($model, 'short_description')->textInput(['maxlength' => true]) ?>
            </div>


        </div>
    </div>


    <div class="col-xs-12">
        <?= $form->field($model, 'description')
            ->widget(
                Redactor::className(),
                [
                    'clientOptions' =>
                        [
                            'imageUpload' => \yii\helpers\Url::to(['/redactor/upload/image']),
                            'fileUpload' => false,
                            'plugins' => ['fontcolor', 'imagemanager', 'table', 'undoredo', 'clips', 'fullscreen'],
                        ],
                ]
            ); ?>
    </div>



    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>