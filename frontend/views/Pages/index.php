<?php
$homeUrl = Yii::$app->homeUrl;
?>
<header class="slider-main">
    <div id="carouselExampleIndicators" class="carousel slide carousel-fade" data-ride="carousel">
        <ol class="carousel-indicators">
            <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner" role="listbox">
            <!-- Slide One - Set the background image for this slide in the line below -->
            <div class="carousel-item active" style="background-image: url('images/slider-01.jpg')">
                <div class="carousel-caption d-none d-md-block">
                    <h3>Welcome to Information programming</h3>
                    <p>Everything for your programming</p>
                </div>
            </div>
            <!-- Slide Two - Set the background image for this slide in the line below -->
            <div class="carousel-item" style="background-image: url('images/slider-02.jpg')">
                <div class="carousel-caption d-none d-md-block">
                    <h3>Welcome to Information programming</h3>
                    <p>Everything for your programming</p>
                </div>
            </div>
            <!-- Slide Three - Set the background image for this slide in the line below -->
            <div class="carousel-item" style="background-image: url('images/slider-03.jpg')">
                <div class="carousel-caption d-none d-md-block">
                    <h3>Welcome to Information programming</h3>
                    <p>Everything for your programming</p>
                </div>
            </div>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
</header>

<div class="container">
    <!-- About Section -->
    <div class="about-main">
        <div class="row">
            <div class="col-lg-6">
                <h2>Welcome to Information programming</h2>
                <p>A programmer is a specialist engaged in the development of software (software) for personal,
                    embedded, industrial and other types of computers, that is, programming. It can be either the head
                    of large software development projects or a “loner” writing code for a specific program.
                    Most enterprises and organizations sooner or later face the question of automation, and many
                    managers strive to use the achievements of modern information technologies, so the programmer is a
                    kind of consultant who performs an intermediary function between what the manager wants to and what
                    the world offers at the moment technology.
                    In those developments where you primarily need the search for creative solutions</p>
            </div>
            <div class="col-lg-6">
                <img class="img-fluid rounded" src="images/about-img.jpg" alt=""/>
            </div>
        </div>
        <!-- /.row -->
    </div>
</div>


<div class="container">
    <!-- Portfolio Section -->
    <div class="portfolio-main">
        <h2>Categories</h2>
        <div class="col-lg-12">

            <div class="project-menu text-center">
                <?php foreach ($categories as $value) { ?>
                    <button class="btn btn-primary"
                            data-filter=".category_<?= $value['id'] ?>"><?= $value['name'] ?></button>
                <?php } ?>
            </div>
        </div>
        <div id="projects" class="projects-main row">
            <?php foreach ($informationAll as $item) { ?>
                <div class="col-lg-4 col-sm-6 pro-item portfolio-item category_<?= $item['category_id'] ?>">
                    <div class="card h-100">
                        <div class="card-img">
                            <img src="<?= $homeUrl ?>images/information/small/<?= $item['img_name'] ?>" alt=""
                            >
                        </div>
                        <div class="card-body">
                            <h4 class="card-title">
                                <a href="#"><h3><?= $item['name'] ?></h3></a>
                            </h4>
                        </div>
                    </div>
                </div>
            <?php } ?>

        </div>
        <!-- /.row -->
    </div>
</div>

<div class="blog-slide">
    <div class="container">
        <h2>Our Information</h2>
        <div class="row">
            <div class="col-lg-12">
                <div id="blog-slider" class="owl-carousel">
                    <?php
                    foreach ($information as $value) {


                        ?>

                        <div class="post-slide">
                            <div class="post-header">

                                <ul class="post-bar">
                                    <li><img src="images/testi_01.png" alt=""><a
                                                href="#"><?= $value['user']['username'] ?></a></li>
                                    <li><i class="fa fa-calendar"></i><?= date('Y-m-d', $value['created_at']) ?></li>
                                </ul>
                            </div>
                            <div class="pic">
                                <img src="<?= $homeUrl ?>images/information/small/<?= $value['img_name'] ?>" alt="">
                                <ul class="post-category">
                                    <li><a href="#"><?= $value['name'] ?></a></li>
                                </ul>
                            </div>
                            <p class="post-description">
                                <?= $value['short_description'] ?>
                            </p>
                        </div>
                        <?php
                    }
                    ?>

                </div>
            </div>
        </div>
    </div>
</div>