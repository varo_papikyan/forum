<?php $homeUrl = Yii::$app->homeUrl;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */

/* @var $model \frontend\models\ContactForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

$this->title = 'Contact';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="full-title">
    <div class="container">
        <!-- Page Heading/Breadcrumbs -->
        <h1 class="mt-4 mb-3"> Contact </h1>
        <div class="breadcrumb-main">
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="<?= $homeUrl ?>">Home</a>
                </li>
                <li class="breadcrumb-item active">Contact</li>
            </ol>
        </div>
    </div>
</div>

<div class="contact-main">
    <div class="container">
        <!-- Content Row -->
        <div class="row">
            <!-- Map Column -->
            <div class="col-lg-3"></div>
            <div class="col-lg-6 mb-4 contact-center">
                <h3 align="center">Send us a Message</h3>
                <form name="sentMessage" id="contactForm" novalidate>
                    <div class="control-group form-group">
                        <div class="controls">
                            <input type="text" placeholder="Full Name" class="form-control" id="name" required
                                   data-validation-required-message="Please enter your name.">
                            <p class="help-block"></p>
                        </div>
                    </div>
                    <div class="control-group form-group">
                        <div class="controls">
                            <input type="tel" placeholder="Phone Number" class="form-control" id="phone" required
                                   data-validation-required-message="Please enter your phone number.">
                        </div>
                    </div>
                    <div class="control-group form-group">
                        <div class="controls">
                            <input type="email" placeholder="Email Address" class="form-control" id="email" required
                                   data-validation-required-message="Please enter your email address.">
                        </div>
                    </div>
                    <div class="control-group form-group">
                        <div class="controls">
                                <textarea rows="5" cols="100" placeholder="Message" class="form-control" id="message"
                                          required data-validation-required-message="Please enter your message"
                                          maxlength="999" style="resize:none"></textarea>
                        </div>
                    </div>
                    <div id="success"></div>
                    <!-- For success/fail messages -->
                    <button type="submit" class="btn btn-primary" id="sendMessageButton">Send Message</button>
                </form>
            </div>
            <!-- Contact Details Column -->

        </div>
        <!-- /.row -->
    </div>
    <!-- /.container -->
</div>


