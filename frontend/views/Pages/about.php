<?php $homeUrl = Yii::$app->homeUrl;
?>
<div class="full-title">
    <div class="container">
        <!-- Page Heading/Breadcrumbs -->
        <h1 class="mt-4 mb-3"> About Us</h1>
        <div class="breadcrumb-main">
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="<?= $homeUrl ?>">Home</a>
                </li>
                <li class="breadcrumb-item active"> About Us</li>
            </ol>
        </div>
    </div>
</div>

<div class="container">
    <!-- About Content -->
    <div class="about-main">
        <div class="row">
            <div class="col-lg-6">
                <img class="img-fluid rounded mb-4" src="<?= $homeUrl ?>images/about-img.jpg" alt=""/>
            </div>
            <div class="col-lg-6">
                <h2>Welcome to Information programming</h2>
                <p>A programmer is a specialist engaged in the development of software (software) for personal,
                    embedded, industrial and other types of computers, that is, programming. It can be either the head
                    of large software development projects or a “loner” writing code for a specific program.
                    Most enterprises and organizations sooner or later face the question of automation, and many
                    managers strive to use the achievements of modern information technologies, so the programmer is a
                    kind of consultant who performs an intermediary function between what the manager wants to and what
                    the world offers at the moment technology.
                    In those developments where you primarily need the search for creative solutions</p>
            </div>
        </div>
        <!-- /.row -->
    </div>
</div>
<div class="about-inner">
    <div class="container">
        <div class="row mb-4">
            <div class="col-lg-6">
                <div class="left-ab">
                    <h2>History</h2>
                    <p> The Antikythera mechanism from Ancient Greece was a calculator that
                        used gears of various sizes and configurations that determined its work [5], for tracking the
                        methone cycle still used in lunar-solar calendars.
                        1206 - Al-Jazari built a programmable humanoid machine. One system used in these devices used
                        clamps and cams placed in a wooden box in certain places, which sequentially engaged levers,
                        which, in turn, controlled percussion instruments [source not specified 744 days].
                        1804 - a jacquard loom was built, built in 1804 by Joseph Marie Jacquard, who revolutionized the
                        weaving industry by providing the ability to program patterns on fabrics using punched cards, it
                        is sometimes considered the first programmable device [source not specified 744 days].
                        July 19, 1843 - Charles Babbage developed (but could not build it) "Analytical Machine" - the
                        first programmable computing device [source not specified 744 days].
                        July 19, 1843 - Countess of Hell Augusta Lovelace, daughter of the English poet George Byron,
                        wrote the first program for the Analytical Machine in the history of mankind. This program
                        solved the Bernoulli equation expressing the law of conservation of energy of a moving fluid. In
                        her first and only scientific work, Ada Lovelace considered a large number of questions. A
                        number of general points expressed by her (the principle of saving working memory cells, the
                        relationship of recurrence formulas with cyclic calculation processes) have retained their
                        fundamental importance for modern programming</p>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="right-ab">
                    <img class="img-fluid rounded mb-4" src="<?= $homeUrl ?>images/mission-img.jpg" alt=""/>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6">
                <div class="right-ab">
                    <img class="img-fluid rounded mb-4" src="<?= $homeUrl ?>images/vision-img.jpg" alt=""/>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="left-ab">
                    <h3>Programming languages</h3>
                    <p>Most of the work of programmers is related to writing source code, testing and debugging programs
                        in one of the programming languages. The source texts and executable files of the programs are
                        copyrighted and are the intellectual property of their authors and copyright holders [source not
                        specified 744 days].
                        Different programming languages ​​support different programming styles (programming paradigms).
                        Choosing the right programming language for some parts of the algorithm can reduce the time it
                        takes to write a program and solve the problem of describing the algorithm most effectively.
                        Different languages ​​require a programmer to pay different attention to details when
                        implementing an algorithm, which often results in a compromise between simplicity and
                        performance (or between programmer time and user time).
                        The only language directly executed by a computer is machine language (also called machine code
                        and machine instruction language). Initially, all programs were written in machine code, but now
                        this is almost not done. Instead, programmers write the source code in one programming language
                        or another, then, using the compiler, translate it in one or several stages into machine code
                        ready for execution on the target processor, or into an intermediate representation that can be
                        executed by a special interpreter - a virtual machine . But this is true only for high-level
                        languages. If full low-level control over the system is required at the level of machine
                        instructions and individual memory cells, the programs are written in assembly language, the
                        mnemonic instructions of which are converted one-to-one into the corresponding machine language
                        instructions of the target computer processor (for this reason, translators from assembly
                        languages ​​are obtained by algorithmically simple translators) .
                        </p>

                </div>
            </div>
        </div>
    </div>
</div>
