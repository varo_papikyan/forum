<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/site.css',
        'css/all.css',
        'css/jquery.fancybox.min.css',
        'css/owl.carousel.min.css',
        'css/style.css',
        'css/bootstrap.min.css',
    ];
    public $js = [
        'js/contact_me.js',
        'js/imagesloaded.pkgd.min.js',
        'js/filter.js',
        'js/isotope.pkgd.min.js',
        'js/jqBootstrapValidation.js',
        'js/jquery.appear.js',
        'js/jquery.fancybox.min.js',
        'js/owl.carousel.min.js',
        'js/script.js',
        'js/bootstrap.bundle.min.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
