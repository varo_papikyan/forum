<?php


namespace frontend\controllers;


use common\models\Categories;
use common\models\Comments;
use common\models\Information;
use Egulias\EmailValidator\Warning\Comment;
use Yii;
use yii\web\Controller;

class CategoriesController extends Controller
{
    public function actionIndex()
    {
        $categories = Categories::find()->where(['is_status' => 1])->asArray()->all();

        return $this->render('index', ['categories' => $categories]);
    }

    public function actionDetails()
    {
        $details = Categories::find()
            ->where(['categories.slug' => $_GET['slug']])
            ->joinWith('information')
            ->asArray()
            ->one();

        return $this->render('details', ['details' => $details]);
    }

    public function actionInformationDetails($slug)
    {
        $information = Information::find()->where(['information.slug' => $slug])->asArray()->one();
        $comments = new Comments();
        $selectComments =
            Comments::find()
                ->where(['information_id'=>$information['id']])
                ->joinWith('user')
                ->asArray()
                ->all();

        if ($comments->load(Yii::$app->request->post()) && !Yii::$app->user->isGuest) {
            $comments->is_status = 1;
            $comments->information_id = $information['id'];
            $comments->user_id = Yii::$app->user->identity->getId();
            $comments->save(false);

            return $this->redirect('information-details?slug=' . $slug);
        }

        return $this->render(
            'information-details',
            [
                'information' => $information,
                'comments' => $comments,
                'selectComments' =>$selectComments,
            ]
        );
    }

}
