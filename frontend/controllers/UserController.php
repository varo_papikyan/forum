<?php


namespace frontend\controllers;


use backend\components\ImageHelper;
use common\models\Information;
use common\models\User;
use Yii;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;

class UserController extends Controller
{
    public function actionIndex()
    {
        if (\Yii::$app->user->isGuest) {
            throw new BadRequestHttpException();
        }

        return $this->render('index');
    }

    public function actionCreateInformation()
    {
        $model = new Information();

        if ($model->load(Yii::$app->request->post())) {
            $imgFile = UploadedFile::getInstance($model, "img_name");

            if (!empty($imgFile)) {
                $image = new ImageHelper();
                $imgPath = Yii::getAlias("@frontend") . "/web/images/information/";
                $imgName = Yii::$app->security->generateRandomString() . '.' . $imgFile->extension;
                $imgFile->saveAs($imgPath . $imgName);

                $image->reSize($imgPath . $imgName, 800, 600);
                $image->reSize($imgPath . $imgName, 400, 300, sprintf('%ssmall/%s', $imgPath, $imgName));

                $model->img_name = $imgName;
            }

            $model->user_id = Yii::$app->user->getId();
            $model->is_status = 1;
            $model->seo_keywords = $model->name;
            $model->seo_description = $model->short_description;

            if ($model->save()) {
                return $this->redirect('/categories/information-details?slug=' . $model->slug);
            }
        }

        return $this->render('create-information', [
            'model' => $model,
        ]);
    }
    public function actionMyInformation(){
        $information = Information::find()
            ->where(['user_id'=>Yii::$app->user->getId()])
            ->asArray()
            ->all();

        return $this->render('my-information',['information'=>$information]);
    }
    public function actionUpdateInformation($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {

            $imgFile = UploadedFile::getInstance($model, "img_name");

            if (!empty($imgFile)) {
                $image = new ImageHelper();
                $imgPath = Yii::getAlias("@frontend") . "/web/images/information/";
                $imgName = Yii::$app->security->generateRandomString() . '.' . $imgFile->extension;
                $imgFile->saveAs($imgPath . $imgName);

                $image->reSize($imgPath . $imgName, 800, 600);
                $image->reSize($imgPath . $imgName, 400, 300, sprintf('%ssmall/%s', $imgPath, $imgName));

                $model->img_name = $imgName;
            }

            if ($model->save()) {
                return $this->redirect('/categories/information-details?slug=' . $model->slug);
            }
        }

        return $this->render('update-information', [
            'model' => $model,
        ]);
    }

    /*
     * Deletes an existing Information model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDeleteInformation($id)
    {
        $model = $this->findModel($id);
//        $imgPath = Yii::getAlias("@frontend") . "/web/images/information/";
//
//        if (file_exists($imgPath.$model->img_name)) {
//            unlink($imgPath.$model->img_name);
//            unlink($imgPath.'small/'.$model->img_name);
//        }

        $model->delete();
        return $this->redirect('/user/my-information');
    }

    /**
     * Finds the Information model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Information the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Information::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

}