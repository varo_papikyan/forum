<?php


namespace frontend\controllers;


use common\models\Categories;
use common\models\Information;
use yii\web\Controller;

class PagesController extends Controller
{
    public function actionIndex()
    {

        $information =
            Information::find()
                ->orderBy(['id' => SORT_DESC])
                ->joinWith('user')
                ->asArray()
                ->limit(8)
                ->all();

        $informationAll =
            Information::find()
                ->orderBy(['id' => SORT_DESC])
                ->limit(12)
                ->asArray()
                ->all();

        $categories =
            Categories::find()
                ->asArray()
                ->all();


        return $this->render('index',
            [
                'information' => $information,
                'categories' => $categories,
                'informationAll' => $informationAll
            ]
        );

    }


    public function actionAbout()
    {
        return $this->render('about');
    }

    public function actionContact()
    {
        return $this->render('contact');
    }


}